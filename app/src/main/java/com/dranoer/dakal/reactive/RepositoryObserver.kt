package com.dranoer.dakal.reactive

import com.google.gson.Gson
import com.dranoer.dakal.data.source.remote.util.*
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import io.reactivex.Observer

/**
 *
 * observer for Repository Observables. handle error and wrapp methodes
 */

abstract class RepositoryObserver<T> : Observer<T> {


    override fun onNext(t: T) {
        if (t == null)
            onNotFound()
        /*else if (t is List<*> && (t as List<*>).isEmpty())
            onNotFound()*/
        else
            onResponse(t)
    }

    override fun onError(e: Throwable) {
        if (e is HttpException) {
            var model: RemoteModel<Any, Any>?
            try {
                model = Gson().fromJson<RemoteModel<Any, Any>>(e.response().errorBody()!!.string(), RemoteModel::class.java)
            } catch (e1: Exception) {
                model = RemoteModel()
            }
            when (e.code()) {
                400 -> {
                    onFailure(ServerLogicErrorException(model?.error))
                }
                404 -> onNotFound()
                409 -> onFailure(DuplicateException(model?.error))
                500 -> onFailure(ServerErrorException(model?.error))
                401 -> onFailure(LoginException(model?.error))
                else -> onFailure(ServerErrorException(model?.error))
            }
        } else if (e is NotFoundException)
            onNotFound()
        else if (e is NullPointerException)
            onNotFound()
        else
            onFailure(ServerErrorException(null))

    }

    override fun onComplete() {

    }


    /*call when success get [data]
    * @param t is [data]
    * */
    abstract fun onResponse(t: T)

    /*call when data not found or is empty*/
    abstract fun onNotFound()

    /*call when fail get data*/
    abstract fun onFailure(e: Throwable)
}
