package com.dranoer.dakal.common

enum class Status
{
    LOADING, SUCCESS, ERROR, NOTFOUND
}