package com.dranoer.dakal.common

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import androidx.annotation.Nullable
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.dranoer.dakal.R
import com.dranoer.dakal.authentication.RegisterActivity
import dagger.android.support.DaggerAppCompatActivity


open class DaggerBaseActivity: DaggerAppCompatActivity()
{


    lateinit var toolbar: Toolbar

    override fun onCreate(@Nullable savedInstanceState: Bundle?, @Nullable persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
    }

    /**
     * add fragment to a container or replace an existing fragment that was added to a container.
     *
     * @param fragment The new fragment to place in the container.
     * @param tag Optional tag name for the fragment, to later retrieve the
     * fragment with [.getFragment]
     * @param backStack Determina that fragment add to backstack  or not
     */
    fun addFragment(fragment: Fragment, tag: String, backStack: Boolean?, containerViewId: Int) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(containerViewId, fragment, tag)
        if (backStack!!)
            fragmentTransaction.addToBackStack(tag)
        fragmentTransaction.commitAllowingStateLoss()
    }

    /**
     * remove an existing fragment that was added.
     *
     * @param fragment The existing fragment to remove.
     */
    fun removeFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().remove(fragment).commit()
        supportFragmentManager.popBackStack()
    }

    /**
     * @return true if fragment by @param tag is available
     *
     * @param tag is tag name of fragment
     */
    fun isAvailableFragment(tag: String): Boolean {
        val fragment = supportFragmentManager.findFragmentByTag(tag)
        return fragment != null
    }

    /**
     * @return fragment added by @param tag
     * @return null if fragment not available
     *
     * @param tag is tag name of fragment
     */
    fun getFragment(tag: String): Fragment? {
        return if (isAvailableFragment(tag))
            supportFragmentManager.findFragmentByTag(tag)
        else
            null
    }

    fun startLogin() {
        val loginIntent = Intent(this, RegisterActivity::class.java)
        startActivityForResult(loginIntent, RegisterActivity.REQUEST)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RegisterActivity.REQUEST) {
            if (resultCode == Activity.RESULT_OK)
                login()
            else
                faildLogin()
        }

    }


    open protected fun setToolbar(resId: Int)
    {
        try {
            toolbar = findViewById(R.id.appbar)
            setSupportActionBar(toolbar)
        }catch (e: Exception)
        {
            return
        }
    }

    public open fun setTitle(titiel: String)
    {
        try {
            toolbar.findViewById<AppCompatTextView>(R.id.title).text = titiel
        }catch (e: Exception)
        {
            return
        }
    }





    open fun login() {};
    open fun faildLogin() {};
}