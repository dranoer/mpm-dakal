package com.dranoer.dakal.common

import android.os.Bundle
import com.dranoer.dakal.common.kprogresshud.KProgressHUD
import com.dranoer.dakal.di.DaggerFragmentX

abstract class FragmentX: DaggerFragmentX()
{

    lateinit var progress: KProgressHUD

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        progress = KProgressHUD.create(activity)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(true)
                .setAutoDismiss(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .setCancellable{ dismis() }
    }

    fun showProgress() = progress.show()

    fun dismisProgress() = progress.dismiss()

    open fun dismis() {  }


}