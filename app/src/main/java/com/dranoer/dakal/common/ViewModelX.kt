package com.dranoer.dakal.common

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class ViewModelX: ViewModel()
{
    val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    open fun unSubscribe()
    {
        compositeDisposable.clear()
    }


}