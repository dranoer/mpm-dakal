package com.dranoer.dakal.authentication

import com.dranoer.dakal.di.ActivityScoped
import dagger.Subcomponent
import dagger.android.AndroidInjector

@ActivityScoped
@Subcomponent(modules = arrayOf(RegisterModule::class))
interface RegisterComponent : AndroidInjector<RegisterActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<RegisterActivity>() {}
}