package com.dranoer.dakal.authentication.login

import com.dranoer.dakal.common.Response
import com.dranoer.dakal.common.SingleLiveEvent
import com.dranoer.dakal.common.ViewModelX
import com.dranoer.dakal.data.model.Error
import com.dranoer.dakal.data.model.Token
import com.dranoer.dakal.data.source.AccountRepository
import com.dranoer.dakal.data.source.remote.util.ApiException
import com.dranoer.dakal.data.source.remote.util.ServerErrorException
import com.dranoer.dakal.di.Repository
import com.dranoer.dakal.reactive.RepositoryObserver
import com.dranoer.dakal.reactive.RxSchedulers
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class LoginViewModel @Inject constructor(@Repository val repository: AccountRepository): ViewModelX() {

    var loginLiveData: SingleLiveEvent<Response<Any>> = SingleLiveEvent()
    var errorLiveData: SingleLiveEvent<List<Error>> = SingleLiveEvent()

    fun login(age: Int,
              username: String,
              appPackageName: String,
              gender: String)
    {
        repository.login(age, username, appPackageName, gender).observeOn(RxSchedulers.main()).subscribeOn(RxSchedulers.io()).subscribe(object : RepositoryObserver<Token>() {
            override fun onNotFound() {
                loginLiveData.postValue(Response.error("", null))
            }

            override fun onResponse(s: Token) {
//                val user = User(true, phone)
//                repository.addAccount(user, s)
                loginLiveData.postValue(Response.success(s))
            }

            override fun onFailure(e: Throwable) {
                if (e is ApiException)
                    when (e) {
                        is ServerErrorException -> {
                            loginLiveData.postValue(Response.error("خطا ارتباط با سرور", null))
                        }
                        else -> {
                            errorLiveData.postValue(e.errors)
                        }
                }
            }

            override fun onSubscribe(d: Disposable) {
                compositeDisposable.add(d)
                loginLiveData.postValue(Response.loading(null))
            }
        })

    }


}