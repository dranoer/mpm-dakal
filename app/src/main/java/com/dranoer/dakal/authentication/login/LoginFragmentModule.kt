package com.dranoer.dakal.authentication.login

import androidx.lifecycle.ViewModel
import com.dranoer.dakal.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class LoginFragmentModule {

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal abstract fun bindLoginViewModel(myViewModel: LoginViewModel): ViewModel

}