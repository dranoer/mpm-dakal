package com.dranoer.dakal.authentication

import android.os.Bundle
import com.dranoer.dakal.R
import com.dranoer.dakal.authentication.login.LoginFragment
import com.dranoer.dakal.common.DaggerBaseActivity

class RegisterActivity : DaggerBaseActivity() {

    companion object {
        val REQUEST = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         setContentView(R.layout.activity_register)


        setToolbar(R.id.appbar)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, LoginFragment.newInstance())
                    .commitNow()
        }
    }

}
