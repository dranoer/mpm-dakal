package com.dranoer.dakal.authentication.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dranoer.dakal.App
import com.dranoer.dakal.R
import com.dranoer.dakal.common.DaggerBaseActivity
import com.dranoer.dakal.common.FragmentX
import com.dranoer.dakal.common.Status
import com.dranoer.dakal.dashboard.DashboardActivity
import com.dranoer.dakal.data.model.Error
import com.dranoer.dakal.di.DaggerViewModelFactory
import kotlinx.android.synthetic.main.login_fragment.*
import javax.inject.Inject


class LoginFragment : FragmentX() {

    companion object {
        fun newInstance() = LoginFragment()
    }

    lateinit var viewModel: LoginViewModel

    @Inject
    lateinit var viewModelFactory: DaggerViewModelFactory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.login_fragment, container, false)



        return view
    }

    override fun onResume() {
        super.onResume()
        (activity as DaggerBaseActivity).setTitle("ورود اطلاعات")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)

        viewModel.loginLiveData.observe(this, Observer {
            when(it.status)
            {
                Status.LOADING -> {
                   //dialog = ProgressDialog.show(context, "","Loading. Please wait...", true)
                    showProgress()
                }
                Status.SUCCESS -> {
                    dismisProgress()

                }
                Status.ERROR -> {
                    dismisProgress()
                    Toast.makeText(App.getContext(), it.message, Toast.LENGTH_SHORT).show()
                }
            }

        })


        viewModel.errorLiveData.observe(this, Observer {
            phoneLayout.error = null
            passwordLayout.error = null
            dismisProgress()
            for (error in it!!)
                when (error.title)
                {
                    Error.PASSWORD -> { passwordLayout.error = error.message[0] }
                    Error.PHONE -> { phoneLayout.error = error.message[0] }
                    else -> { Toast.makeText(App.getContext(), error.message[0], Toast.LENGTH_SHORT).show() }
                }
        })


        login.setOnClickListener {
            viewModel.login(15, "testuser@gmail.com", "fakeApp", "0")


            val intent = Intent(context, DashboardActivity::class.java)
            startActivity(intent)
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == Activity.RESULT_OK)
        {
//            val intent = Intent(context, DashboardActivity::class.java)
//            startActivity(intent)
//            activity?.finish()
        }

    }


    override fun dismis()
    {
        viewModel.unSubscribe()
    }

}

