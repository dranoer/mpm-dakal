package com.dranoer.dakal.authentication

import com.dranoer.dakal.authentication.login.LoginFragment
import com.dranoer.dakal.authentication.login.LoginFragmentModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class RegisterProvider {

    @ContributesAndroidInjector(modules = arrayOf(LoginFragmentModule::class))
    abstract fun provideLoginFragment(): LoginFragment

}