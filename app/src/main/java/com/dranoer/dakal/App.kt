package com.dranoer.dakal

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatDelegate
import com.facebook.stetho.Stetho
import com.dranoer.dakal.di.DaggerAppComponent
import com.dranoer.dakal.data.source.remote.RetrofitBuilder
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = DaggerAppComponent.builder().application(this).build()

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreate() {
        super.onCreate()
        App.appContext = applicationContext
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        RetrofitBuilder.init()
        Stetho.initializeWithDefaults(this)
    }

    companion object {
        var appContext: Context? = null
            private set
        fun getContext() : Context? = appContext
    }
}