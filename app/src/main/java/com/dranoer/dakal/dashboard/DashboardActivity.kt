package com.dranoer.dakal.dashboard

import android.os.Bundle
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.dranoer.dakal.R
import com.dranoer.dakal.dashboard.frag_one.FirstFragment
import com.dranoer.dakal.dashboard.frag_three.ThirdFragment
import com.dranoer.dakal.dashboard.frag_two.SecondFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class DashboardActivity : AppCompatActivity() {

    lateinit var toolbar: ActionBar
    var toggle: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        toolbar = supportActionBar!!
        val bottomNavigation: BottomNavigationView =
                findViewById(R.id.navigationView)

        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        if (!toggle) {
            val puzzle = FirstFragment.newInstance()
            openFragment(puzzle)
        }
    }


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_puzzle -> {
                toolbar.title = "پازل"
                val puzzle = FirstFragment.newInstance()
                openFragment(puzzle)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_face -> {
                toolbar.title = "لبخند"
                val face = SecondFragment.newInstance()
                openFragment(face)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_coloring -> {
                toolbar.title = "نقاشی"
                val coloring = ThirdFragment.newInstance()
                openFragment(coloring)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
        toggle == true
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
//        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}