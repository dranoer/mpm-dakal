package com.dranoer.dakal.dashboard.frag_three

import android.content.Intent
import android.os.Bundle
//import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dranoer.dakal.R
//import com.dranoer.dakal.R
//import com.dranoer.dakal.R.id.submitThird
//import com.dranoer.dakal.games.coloring.ColoringActivity
//import com.dranoer.dakal.games.face.FaceTrackerActivity
import com.dranoer.dakal.dashboard.frag_one.FirstFragment
import com.dranoer.dakal.games.coloring.ColoringActivity
//import com.dranoer.dakal.games.puzzle.PuzzleActivity
import kotlinx.android.synthetic.main.fragment_first.*
import kotlinx.android.synthetic.main.fragment_third.*

class ThirdFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_third, container, false)

    companion object {
        fun newInstance(): ThirdFragment = ThirdFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view!!, savedInstanceState)


        submitThird.setOnClickListener {
            val intent = Intent(context, ColoringActivity::class.java)
            startActivity(intent)
        }
    }
}