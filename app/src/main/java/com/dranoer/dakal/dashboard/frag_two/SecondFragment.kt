package com.dranoer.dakal.dashboard.frag_two

import android.content.Intent
import android.os.Bundle
//import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dranoer.dakal.R
//import com.dranoer.dakal.R
//import com.dranoer.dakal.games.face.FaceTrackerActivity
import com.dranoer.dakal.dashboard.frag_one.FirstFragment
import com.dranoer.dakal.games.face.FaceTrackerActivity
//import com.dranoer.dakal.games.puzzle.PuzzleActivity
import kotlinx.android.synthetic.main.fragment_first.*
import kotlinx.android.synthetic.main.fragment_second.*

class SecondFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_second, container, false)

    companion object {
        fun newInstance(): SecondFragment = SecondFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view!!, savedInstanceState)

        submitSecond.setOnClickListener {
            val intent = Intent(context, FaceTrackerActivity::class.java)
            startActivity(intent)
        }
    }
}