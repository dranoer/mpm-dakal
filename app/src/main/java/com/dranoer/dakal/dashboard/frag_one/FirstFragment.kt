package com.dranoer.dakal.dashboard.frag_one

import android.content.Intent
import android.os.Bundle
//import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.dranoer.dakal.R
import com.dranoer.dakal.games.puzzle.PuzzleActivity
import kotlinx.android.synthetic.main.fragment_first.*

class FirstFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_first, container, false)

    companion object {
        fun newInstance(): FirstFragment = FirstFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view!!, savedInstanceState)

        submitFirst.setOnClickListener {
            val intent = Intent(context, PuzzleActivity::class.java)
            startActivity(intent)
        }
    }

}