package com.dranoer.dakal.games.coloring;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.dranoer.dakal.R;

import java.util.ArrayList;

public class Toolkit {

    private Context mContext;
    private ArrayList<Tool> mTools;
    private Tool mCurrentTool;
    private LinearLayout mContainer;
    private OnToolSetListener mCallback;

    public Toolkit(Context context, LinearLayout container, OnToolSetListener listener) {
        mContext = context;
        mContainer = container;
        mTools = new ArrayList<>();
        mCallback = listener;
    }

    void init() {
        mTools.add(new Fill());

        mCurrentTool = mTools.get(0);
        mCurrentTool.resetColor();
        mCallback.draw(true);
    }


    public Tool getSelectedTool() {
        return mCurrentTool;
    }

    public interface OnToolSetListener {
        void draw(boolean fillMode);
        void clear();
    }

    private class Fill extends Tool {
        public Fill() {
            mBtn = new ImageButton(mContext);
            mBtn.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            mBtn.setImageResource(R.drawable.fill);
            mBtn.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            mCurrentTool.resetColor();
            mCurrentTool = this;
            mCallback.draw(true);
        }
    }
}