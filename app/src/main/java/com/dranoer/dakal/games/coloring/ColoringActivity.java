package com.dranoer.dakal.games.coloring;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.content.ContextCompat;
//import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.dranoer.dakal.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ColoringActivity extends AppCompatActivity implements Palette.OnColorChosenListener, Toolkit.OnToolSetListener {
    private CanvasView mCanvasView;
    private Palette mPalette;
    private Toolkit mToolkit;
    @BindView(R.id.off_ticket)
    ImageView off_ticket;
    @BindView(R.id.paint)
    FrameLayout paintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coloring);
        ButterKnife.bind(this);
        initTools();

        String[] permissionsToAsk = checkPermissions(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET
                , Manifest.permission.INTERNET);
        if (permissionsToAsk.length > 0)
            ActivityCompat.requestPermissions(this, permissionsToAsk, 1);


        Glide.with(this)
                .asBitmap()
                .load("https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Bank_Mellat_logo.svg/992px-Bank_Mellat_logo.svg.png")
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        Bitmap bitmap = getResizedBitmap(resource,800,900);
                        mCanvasView.onReloadedBitmap(bitmap);
                    }
                });
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }


    private void initTools() {
        mCanvasView = (CanvasView) findViewById(R.id.canvas);
        LinearLayout container = (LinearLayout) findViewById(R.id.bottom_tools);
        mPalette = new Palette(this, container, this);

        mToolkit = new Toolkit(this, (LinearLayout) findViewById(R.id.toolkit), this);
        mToolkit.init();
    }

    @Override
    public void draw(boolean fillMode) {
        Tool tool = mToolkit.getSelectedTool();
        tool.setColor(mPalette.getPaintColor());
        mCanvasView.setPaintColor(tool.getPaintColor());
        mCanvasView.setStrokeWidth(tool.getStroke());
        mCanvasView.fill(fillMode);
    }

    public void onFinish(View v) {
        paintLayout.animate()
                .translationX(paintLayout.getHeight())
                .alpha(0.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        paintLayout.setVisibility(View.GONE);
                    }
                })
                .setDuration(750);
    }

    public static String[] checkPermissions(Context cntx, String... permissions) {
        // Check permissions.
        List<String> permissionsToAsk = new ArrayList<>();
        for (String permission : permissions) {
            boolean granted = ContextCompat.checkSelfPermission(Objects.requireNonNull(cntx), permission) == PackageManager.PERMISSION_GRANTED;
            if (!granted)
                permissionsToAsk.add(permission);
        }

        String[] arrPermissions = new String[permissionsToAsk.size()];
        arrPermissions = permissionsToAsk.toArray(arrPermissions);

        return arrPermissions;
    }

    @Override
    public void clear() {
        mCanvasView.clear();
    }

    @Override
    public void updateToolColor(int color) {
        Tool current = mToolkit.getSelectedTool();
        current.setColor(color);
        mCanvasView.setPaintColor(current.getPaintColor());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}