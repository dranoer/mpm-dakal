package com.dranoer.dakal.utils;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

public abstract class WengerViewHolder<T extends Object> extends RecyclerView.ViewHolder
{
    public WengerViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bind(int position, T t);

    public void unBind(){};
}
