package com.dranoer.dakal.utils;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;


public abstract class WengerAdapter<T extends Object, VH extends WengerViewHolder<T>> extends RecyclerView.Adapter<VH> {

    public List<T> list;
    ItemActionListener itemActionListener;

    public static final int CLICK = 100;


    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public boolean isEmpty()
    {
        if (list == null || list.size() == 0)
            return true;
        return false;
    }

    public T get(int index)
    {
        if (list == null)
            return null;
        return list.get(index);
    }


    public void remove(int i)
    {
        if (list == null)
            return;
        list.remove(i);
        notifyItemRemoved(i);
    }

    public void remove(T t)
    {
        if (list == null)
            return;
        int notify = list.indexOf(t);
        list.remove(t);
        notifyItemRemoved(notify);
    }

    public void add(T t)
    {
        if (list == null)
            list = new ArrayList<>();
        list.add(t);
        notifyItemInserted(list.size() - 1);
    }

    public void add(int i, T t)
    {
        if (list == null)
            list = new ArrayList<>();
        list.add(i, t);
        notifyItemInserted(i);
    }


    public ItemActionListener getItemActionListener() {
        return itemActionListener;
    }

    public void setItemActionListener(ItemActionListener itemActionListener) {

        this.itemActionListener = itemActionListener;
    }

    public void setAction(int position, int itemAction, View view)
    {
        if (itemActionListener != null)
            itemActionListener.action(position, itemAction, view);
    }


    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.bind(position, list.get(position));
    }

    @Override
    public void onViewRecycled(VH holder) {
        super.onViewRecycled(holder);
        holder.unBind();
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        return list.size();
    }
}
