package com.dranoer.dakal.utils;

import android.view.View;

public interface ItemActionListener
{
    void action(int index, int action, View view);
}
