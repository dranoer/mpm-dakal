package com.dranoer.dakal.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.dranoer.dakal.dashboard.frag_one.FirstFragment
import com.dranoer.dakal.dashboard.frag_three.ThirdFragment
import com.dranoer.dakal.dashboard.frag_two.SecondFragment

class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                FirstFragment()
            }
            1 -> SecondFragment()
            else -> {
                return ThirdFragment()
            }
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "01"
            1 -> "02"
            else -> {
                return "03"
            }
        }
    }
}