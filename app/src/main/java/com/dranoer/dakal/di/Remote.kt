package com.dranoer.dakal.di

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

import javax.inject.Qualifier

/**
 * @Remote annotation for injection Remote repositories.
 * this is Signature of Rocal repository
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
annotation class Remote
