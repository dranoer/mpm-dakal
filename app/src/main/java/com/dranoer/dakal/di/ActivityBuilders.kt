package com.dranoer.dakal.di

import com.dranoer.dakal.authentication.RegisterActivity
import com.dranoer.dakal.authentication.RegisterModule
import com.dranoer.dakal.authentication.RegisterProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilders {

    @ContributesAndroidInjector(modules = arrayOf(RegisterModule::class, RegisterProvider::class))
    internal abstract fun bindRegisterActivity(): RegisterActivity

}

