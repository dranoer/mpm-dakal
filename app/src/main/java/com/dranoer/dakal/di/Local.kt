package com.dranoer.dakal.di

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

import javax.inject.Qualifier

/**
 * @Local annotation for injection local repositories.
 * this is Signature of local repository
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
annotation class Local
