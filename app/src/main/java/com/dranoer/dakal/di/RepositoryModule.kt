package com.dranoer.dakal.di

import com.dranoer.dakal.data.source.*
import com.dranoer.dakal.data.source.repository.*


import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * add repository providers here
 */

@Module
abstract class RepositoryModule
{

    @Binds
    @Singleton
    @Repository
    internal abstract fun provideAccountRepository(repository: AccountRepositoryImp): AccountRepository

}

