package com.dranoer.dakal.di

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

import javax.inject.Qualifier

/**
 * @Repository annotation for injection main node repositories.
 * this is Signature of main node repository
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
annotation class Repository
