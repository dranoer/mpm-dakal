package com.dranoer.dakal.data.source.remote

import com.dranoer.dakal.data.model.Token
import com.dranoer.dakal.data.source.remote.util.RemoteModel
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 *
 * API endpoints.
 */

interface WebApi
{

    @Headers("Accept: application/json", "Content-type:application/json")

    @POST("GetAdvertisementForDemo")
    fun login(@Body token: Token): Observable<RemoteModel<Token, Any>>

}

