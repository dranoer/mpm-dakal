package com.dranoer.dakal.data.source.remote.util

import com.dranoer.dakal.data.model.Error

/**
 * Exception : not found server error
 */

class NotFoundException(errors: List<Error>?) : ApiException(errors)
