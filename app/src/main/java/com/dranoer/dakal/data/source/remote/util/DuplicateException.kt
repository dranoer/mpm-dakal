package com.dranoer.dakal.data.source.remote.util

import com.dranoer.dakal.data.model.Error

class DuplicateException(errors: List<Error>?) : ApiException(errors)