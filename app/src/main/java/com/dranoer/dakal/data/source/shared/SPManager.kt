package com.dranoer.dakal.data.source.shared

import android.content.Context
import android.content.SharedPreferences
import com.dranoer.dakal.App
import com.dranoer.dakal.BuildConfig
import java.util.*

class SPManager {

    internal var sharedPreferences: SharedPreferences
    internal var editor: SharedPreferences.Editor
    var uniqueID: String? = null


    init {
        sharedPreferences = App.getContext()!!.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
    }

    companion object {

        private var ourInstance: SPManager? = null

        val instance: SPManager
            get() {
                if (ourInstance == null)
                    ourInstance = SPManager()
                return ourInstance as SPManager
            }
    }

    fun setToken(token: String) {
        editor.putString("token", token)
        editor.apply()
    }

    fun setUserName(uname: String?) {
        editor.putString("user_name", uname)
        editor.apply()
    }

    fun setUserID(id: String) {
        editor.putString("user_id", id)
        editor.apply()
    }


    fun getUserID(): String {
        return sharedPreferences.getString("user_id", "3")
    }


    fun getToken(): String {
        return "Bearer  " + sharedPreferences.getString("token", "")!!
        //return "Bearer  eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIsImlzcyI6Imh0dHA6Ly9oYXJhamluby5pc2F0ZWxjby5jb20vYXBpL3YxL3VzZXIvcmVnaXN0ZXIvdmVyaWZ5IiwiaWF0IjoxNTI0MDQ3OTY0LCJleHAiOjUxMjQwNDc5NjQsIm5iZiI6MTUyNDA0Nzk2NCwianRpIjoiRmhrVVFMZlVyc09oZFNacSJ9.I5Q8Stvfv_466v7I_qK81Uj_XncnyxFcZhLNyO66730";
    }


    fun getUserName(): String {
        return sharedPreferences.getString("user_name", "")
    }

    @Synchronized
    fun getUuid(): String? {
        uniqueID = sharedPreferences.getString("UUID", null)
        if (uniqueID == null) {
            uniqueID = UUID.randomUUID().toString()
            editor.putString("UUID", uniqueID)
            editor.commit()
        }
        return uniqueID
    }

}
