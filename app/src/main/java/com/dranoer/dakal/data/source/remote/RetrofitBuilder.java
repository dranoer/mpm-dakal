package com.dranoer.dakal.data.source.remote;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilder
{

    private static final String baseUrl = "http://192.168.1.122/dakal/api/User/";
    public static RetrofitBuilder INSTANCE;
    public static WebApi webApi;
    public static WebApi mockWebApi;


    private RetrofitBuilder() {
    }

    public static void init(){
        if (INSTANCE == null) {

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .addInterceptor(new Interceptor() {
                        @Override
                        public okhttp3.Response intercept(Chain chain) throws IOException {


                            Request original = chain.request();

                            Request request = original.newBuilder()
//                                    .header("uuid", /*SPManager.Companion.getInstance().getUuid()*/"")
                                    .build();

                            return chain.proceed(request);
                        }
                    })
                    .addInterceptor(logging)
                    .addNetworkInterceptor(new StethoInterceptor())
                    .readTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .build();

            webApi = retrofit.create(WebApi.class);
            //----

            INSTANCE = new RetrofitBuilder();
        }
    }

    public static WebApi getWebApi() {
        return webApi;
    }

}
