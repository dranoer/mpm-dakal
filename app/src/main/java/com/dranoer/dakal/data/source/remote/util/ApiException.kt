package com.dranoer.dakal.data.source.remote.util

import com.dranoer.dakal.data.model.Error

open class ApiException(var errors: List<Error>?): RuntimeException() {
}