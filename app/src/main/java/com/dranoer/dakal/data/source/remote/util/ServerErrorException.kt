package com.dranoer.dakal.data.source.remote.util

import com.dranoer.dakal.data.model.Error

/**
 *
 * Exception : server errors (400, 503 and ...)
 */

class ServerErrorException(errors: List<Error>?) : ApiException(errors)
