package com.dranoer.dakal.data.model

data class Error(val title: String, val message: List<String>) {


    companion object {
        val PASSWORD = "password"
        val PHONE = "phone"
        val NO_BMI = "bmi"
        val DESCRIPTION = "description"
    }

}