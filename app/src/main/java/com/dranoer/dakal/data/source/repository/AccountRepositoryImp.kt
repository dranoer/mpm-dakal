package com.dranoer.dakal.data.source.repository

import com.dranoer.dakal.data.model.Token
import com.dranoer.dakal.data.source.AccountRepository
import com.dranoer.dakal.data.source.remote.RetrofitBuilder
import com.dranoer.dakal.data.source.remote.WebApi
import com.dranoer.dakal.data.source.remote.util.RemoteModel
import io.reactivex.Observable
import javax.inject.Inject

class AccountRepositoryImp @Inject constructor() : AccountRepository {

//    var spManager: SPManager? = SPManager.instance

    lateinit var api: WebApi

    init {
        api = RetrofitBuilder.getWebApi()
    }

    override fun login(age: Int, username: String, appPackageName: String, gender: String): Observable<Token> {
        return api.login(Token(age, username, appPackageName, gender)).flatMap { t: RemoteModel<Token, Any> ->  Observable.just(t.data)}
    }

}
