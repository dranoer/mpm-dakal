package com.dranoer.dakal.data.source.remote.util

import com.dranoer.dakal.data.model.Error

/**
 * Generic class of json recieved from API.
 * this class used in [retrofit2.Response]<RemoteModel>
 *
</RemoteModel> */

class RemoteModel<T, E> {

    /*main data of json*/
    var data: T? = null

    /*meta data of json*/
    var meta: E? = null

    /*error of json*/
    var error: List<Error>? = null

}