package com.dranoer.dakal.data.model

data class Token(val age: Int,
                 val username: String,
                 val appPackageName: String,
                 val gender: String) {

    companion object {
        val AGE = 15
        val USERNAME = "testuser@gmail.com"
        val APPPACKAGENAME = "fakeApp"
        val GENDER = "0"
    }
}