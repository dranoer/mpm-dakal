package com.dranoer.dakal.data.source

import com.dranoer.dakal.data.model.Token
import io.reactivex.Observable

interface AccountRepository {

    fun login(age: Int, username: String, appPackageName: String, gender: String): Observable<Token>

}
